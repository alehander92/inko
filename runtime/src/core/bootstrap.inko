# Importing of both the prelude and the bootstrap modules will not yet work at
# this point, hence these are disabled.
![import_bootstrap: false]
![import_globals: false]
![import_prelude: false]

# Implicit module definitions will not yet work since we haven't defined the
# required objects just yet.
#
# As a result of this setting all constants and methods defined in this module
# are defined directly on the top-level object.
![define_module: false]

# Set up the various built-in objects that Inko comes with. These objects need
# to be set manually since the "object" keyword can not yet be used at this
# point.
let Boolean = _INKOC.get_boolean_prototype
let True = _INKOC.get_true
let False = _INKOC.get_false
let Nil = _INKOC.get_nil

let Object = _INKOC.get_object_prototype
let String = _INKOC.get_string_prototype
let Integer = _INKOC.get_integer_prototype
let Float = _INKOC.get_float_prototype
let Block = _INKOC.get_block_prototype
let Array = _INKOC.get_array_prototype

_INKOC.set_object_name(Boolean, 'Boolean')
_INKOC.set_object_name(True, 'True')
_INKOC.set_object_name(False, 'False')
_INKOC.set_object_name(Nil, 'Nil')
_INKOC.set_object_name(Object, 'Object')
_INKOC.set_object_name(String, 'String')
_INKOC.set_object_name(Integer, 'Integer')
_INKOC.set_object_name(Float, 'Float')
_INKOC.set_object_name(Array, 'Array')
_INKOC.set_object_name(Block, 'Block')

# Now that our core objects are set up we can start defining more of the
# building blocks of Inko, such as "Object.new" and the bits necessary to allow
# creating of modules.
impl Object {
  def new -> Self {
    let obj = _INKOC.set_object(False, self)

    obj.init

    obj
  }

  def init {}
}

## A trait.
##
## A trait can contain both required methods and default implementations of
## methods.
object Trait {}

## An object to use for storing all modules created from source files.
object Modules {}

## An object containing methods and types defined in a source file.
##
## This object does not define any meaningful methods on its own, as those could
## conflict with the methods defined in a module by the user. Instead, use the
## `std::mirror` module to obtain module information such as its name or file
## path.
object Module {
  def new(name: String, path: String) -> Self {
    # Modules are always global objects, so we create them as such right away.
    let obj = _INKOC.set_object(True, self)

    obj.init(name: name, path: path)

    obj
  }

  def init(name: String, path: String) {
    let @name = name
    let @path = path
  }
}

# Now we can define the object for the bootstrap module.
_INKOC.set_attribute(
  Modules,
  'core::bootstrap',
  Module.new(name: 'core::bootstrap', path: _INKOC.current_file_path),
)
