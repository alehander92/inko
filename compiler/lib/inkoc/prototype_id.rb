# frozen_string_literal: true

module Inkoc
  module PrototypeID
    OBJECT = 0
    INTEGER = 1
    FLOAT = 2
    STRING = 3
    ARRAY = 4
    BLOCK = 5
    BOOLEAN = 6
    READ_ONLY_FILE = 7
    WRITE_ONLY_FILE = 8
    READ_WRITE_FILE = 9
    BYTE_ARRAY = 10
    HASHER = 11
    LIBRARY = 12
    FUNCTION = 13
    POINTER = 14
  end
end
